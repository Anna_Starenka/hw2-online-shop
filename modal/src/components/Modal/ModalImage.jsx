import ModalBody from "./ModalBody"
import ModalFooter from "./ModalFooter"
import ModalHeader from "./ModalHeader"
import ModalWrapper from './ModalWrapper'
import ModalClose from './ModalClose'
import Modal from "./Modal"




export default  function ModalImage({className, title, text, onClose}){

    return(
    <>
        <ModalWrapper onClose={onClose}>
            <Modal>
                <ModalHeader>
                    <ModalClose onClick={onClose}/>
                </ModalHeader>
                <ModalBody>
                    <div className={className}></div>
                    <h2>{title}</h2>
                    <p>{text}</p>
                </ModalBody>
                <ModalFooter
                    firstText = "NO, CANCEL"
                    secondaryText ="YES, DELETE"
                    firstClick ={onClose}
                    secondaryClick ={onClose}
                />
            </Modal>
        </ModalWrapper>
    </>
    )
}